Welcome to RTFM!
================

RTFM is a documentation related to various IT subjects that can give an idea
on which steps to follow to achieve some particular tasks.

Objectives
----------

You arrive in a small or bigger company where nothing exists or you need to
start from scratch, what would you do? All decisions and changes should be
documentated here.

- Architecture
- redundancy
- high availability

- Prod
- staging

Fully automatically deployement on a cloud infrastructure a complete network
with:

.. todo::

  - BaseOS

    - naming convention
    - SKEL
    - MOTD
    - basic packages
  - DNS server
  - LDAP
  - SSO
  - Backup and restoration
  - laptop/tower automatic installation and distant update
  - VPN access
  - Security 

    - motd
    - firewall (and a port management system centralized to open or close them)
    - 2FA
    - SNMP
    - password management and policies
    - patch, migration and update policy
    - Threat detection, itrusion detection
    - ...
  - Logging [kibana]
  - ACL (to access different things, services)
  - monitoring and paging (of machines, VMs, services)
  - status page for services
  - services:

    - annuaire
    - HR system
    - large file sharing
    - Artifact repository manager [artifactory]
    - images building farm [gitlab-ci]
    - code versionning [gitlab]
    - CI/CD and info on DevOps for code, docker images, ... [gitlab-ci]
    - communication tools [mattermost]
    - Issue manager [alternative to Jira]
    - documentation manager [sphinx/Gitlab pages]
    - email server
    - ML pipelines (automatic creation of computing nodes when pushing to the pipeline)

      - DVC or Pachyderm
      - Binder with Jupyter notebooks
  - to be continued...

.. toctree::

   introduction
   documentation
   bare-metal
   ci-cd
   annexes/index
   notes/index

   glossary
   bibliography


.. Here some idea on the content should be organized.
     - documentation
     - Network
     - Virtualization
     - Containarization
     - Automation
     - Backups
     - status page
     - monitoring
     - Design and create your own network architecture
     - Annexes (VIm configuration, SSH configuration, ...)

.. include:: todo.rst
