Cassandra: NoSQL thanks
=======================

Developped in Java, Cassandra is a NoSQL database system that is popular for
being scalable and with high availability.

## Vocabulary

- keyspace: corresponds to a database in the the RDBMS world
- ring: corresponds to a cluster of instances
- repair:

## Basic commands

.. code-block:: mysql

  # Create a keyspace
  CREATE KEYSPACE <name> WITH replications = { 'class': <replication_strategy>, 'replication_factor': <replication_factor> };


Now if you need to perform operations inside the keyspace, you first need to
access it through the :code:`USE <keyspace>;` command and then execute the following
commands.

.. code-block:: mysql

  # Create a table
  CREATE TABLE <table_name> (<fieldname1> <datatype> <option>, <field_name2> <datatype> <option>, ...);

  # Select everything from a table
  SELECT * FROM <table_name>;
