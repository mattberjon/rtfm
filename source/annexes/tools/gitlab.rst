Gitlab
======

I use Gitlab Community Edition.

Issue work flow
---------------

The community edition doesn't have real issue manager but has an issue system
that suffice for most people like me. It's always good to find a correct way to
document everything correctly. I suggest the following workflow (for a single
person work but obviously can be extended as needed):

- open (corresponds to the backlog)
- To Do (can corresponds to a list of issues for a sprint)
- In progress
- closed
- reopened
- not relevant

It could be useful to keep track of all issues to review them from time to time
but it's always good to clean the backlog from time to time. If an issue becomes
irrelevant at a time, just close it and it as such.

Something to keep in mind. As you need to document everything, a good practice
would be to add comments all along in the open issue and then create the
corresponding and clean documentation if necessary.

Every work you're doing should have a related issue opened before starting to
work and be updated all along the task life span.
