Misc
====

Give right permissions to a group on a specific directory
---------------------------------------------------------

.. warning::

  - Don't forget to create the user if necessary with adduser or useradd command
  - If need be, don't forget to give the ownership and permission recusirvely
    using the -R property.

.. code:: bash

  # Create a groupe
  groupadd <group_name>

  # Give group ownership to the directory
  chown :<group_name> <path>

  # Give write access to the group
  chmod g+w <path>

  # Add the user to the group
  usermod -aG <group_name> <username>


Create a bootable key
---------------------

.. code:: bash

  # You can find the right name
  lsblk -p
  
  NAME                        MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
  /dev/sda                      8:0    0 238.5G  0 disk
  ├─/dev/sda1                   8:1    0   200M  0 part /boot/efi
  ├─/dev/sda2                   8:2    0     1G  0 part /boot
  └─/dev/sda3                   8:3    0 237.3G  0 part
    ├─/dev/mapper/fedora-root 253:0    0    50G  0 lvm  /
    ├─/dev/mapper/fedora-swap 253:1    0   7.9G  0 lvm  [SWAP]
    └─/dev/mapper/fedora-home 253:2    0 179.5G  0 lvm  /home
  /dev/mmcblk0                179:0    0  29.8G  0 disk
  └─/dev/mmcblk0p1            179:1    0  29.8G  0 part

  # Alternatively you can use as well
  fdisk -l
  
  # Recreate the right paritions if necessary by deleting the existing ones
  # and recreate only one Linux primary partition
  fdisk /dev/<volume_name>

  # Format the partition
  mkfs.vfat /dev/<partition_name>

  # Install the ISO on the partition
  dd bs=4M if=</path/to/the/image/file> of=/dev/<volume_name> status=progress conv=fsync


Difference between the volume name and partition name. For example, if you have
a USB key located in `/dev/sdx`:

- the volume name will be `sdx`
- the partition name will be `sdx1`, `sdx2`, etc.
