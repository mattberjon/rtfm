OpenGPG
=======

Most people aren't aware of keys or equivalent cryptographic system used to
communicate (certificates, PKIs, so and so forth). OpenGPG keys are part of the
asymmetric cryptographic system that are used to both encrypt data and sign
them.

Asymetric encryption
--------------------

.. todo::

  Describe shortly how asymetric encryption works.

Configuring GnuPG
-----------------

You can use :term:`GPG` as it is but it's always a good thing to configure it
properly to minimise mistakes.

.. raw:: html

  <script src="https://framagit.org/snippets/4618.js"></script>

Master key and sub-keys
-----------------------

When you will use GPG in a basic mode, you will create a basic pair of keys
that will allow you to both sign and certify content. The main issue with that
being that if you're private key is stolen or lost, you will have to recreate a
pair of keys from scratch. Using sub-keys means that you have a master key that
will be used to create and certify other subkeys. In case you have compromised
subkeys, you have to revoke them and create a new one.

Creating the master key
^^^^^^^^^^^^^^^^^^^^^^^

This key will used to create subkeys. It's important to keep it safely
somewhere, preferably on an offline support (USB key, CD, paper or all of
them). In case this key is stolen or compromised, all your subkeys will be
compromised as well.

.. code:: bash

  $ gpg2 --expert --full-gen-key

  gpg (GnuPG) 2.1.11; Copyright (C) 2016 Free Software Foundation, Inc.
  This is free software: you are free to change and redistribute it.
  There is NO WARRANTY, to the extent permitted by law.

  Please select what kind of key you want:
     (1) RSA and RSA (default)
     (2) DSA and Elgamal
     (3) DSA (sign only)
     (4) RSA (sign only)
     (7) DSA (set your own capabilities)
     (8) RSA (set your own capabilities)
     (9) ECC and ECC
    (10) ECC (sign only)
    (11) ECC (set your own capabilities)
  Your selection? 8

We want to create the master key that will certify all the other subkeys.
That's why we select the menu 8 using the RSA algorithm.

.. code:: bash

  Possible actions for a RSA key: Sign Certify Encrypt Authenticate
  Current allowed actions: Sign Certify Encrypt

     (S) Toggle the sign capability
     (E) Toggle the encrypt capability
     (A) Toggle the authenticate capability
     (Q) Finished

  Your selection? s

  Possible actions for a RSA key: Sign Certify Encrypt Authenticate
  Current allowed actions: Certify Encrypt

     (S) Toggle the sign capability
     (E) Toggle the encrypt capability
     (A) Toggle the authenticate capability
     (Q) Finished

  Your selection? e

  Possible actions for a RSA key: Sign Certify Encrypt Authenticate
  Current allowed actions: Certify

     (S) Toggle the sign capability
     (E) Toggle the encrypt capability
     (A) Toggle the authenticate capability
     (Q) Finished

  Your selection? q

We have configured the capabilities of this first key to allow only
certification. Let’s move on to the size of the key: it is recommended  to have
a key of a minimum size of 2048. To this day, this length is  still resistant,
but it is preferable to take the maximum size: *4096*.

For  the lifetime of the key, it is always recommended to put one. If this  key
is lost, and it has been sent to a key server, it will remain there  forever
valid. Put a duration up to 2 years. Here I will put 1 year.  This allows you
to practice command lines every year :).

.. code:: bash

  RSA keys may be between 1024 and 4096 bits long.
  What keysize do you want? (2048) 4096
  Requested keysize is 4096 bits
  Please specify how long the key should be valid.
           0 = key does not expire
        <n>  = key expires in n days
        <n>w = key expires in n weeks
        <n>m = key expires in n months
        <n>y = key expires in n years
  Key is valid for? (0) 1y
  Key does not expire at all
  Is this correct? (y/N) y

.. todo::

  - Renew the keys
  - How to use it with files
  - How to use it with emails
  - Backup your key pair and contact public keys
  - Some useful commands
