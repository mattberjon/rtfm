Sphinx: documentation tool
==========================

You have many documentation tools available on the internet and can choose
whatever suits you most. I suggest here the one I use for this book: Sphinx.

I'm familiar with Python, ReStructuredText is a powerful markup language and
this tool is well adapted to long books even though themes aren't that great.

.. Todo::
   - Explain how to use Sphinx
   - how to manage images
   - integration of Gitlab snippets
   - ...
