Git: the dev tool
=================

Impossible to have a technical work done without working with a modern
:term:`DVCS`.

Git Configuration
-----------------

You can access the Git configuration in the `~/gitconfig`.

.. todo::

  - better description of each configuration
  - add several useful aliases
  - add configuration for submodule

.. raw:: html

  <script src="https://framagit.org/snippets/5152.js"></script>

Useful commands
---------------

You'll find below some useful commands that are always good to know.

.. code-block:: bash

    # Cancel your last commit but keep the changes
    git reset --soft HEAD~1

    # Cancel your last commit and its changes
    git reset --hard HEAD~1
