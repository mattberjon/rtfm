SSH
===

Some Ed25519 Benefits
---------------------
.. todo::

  Rewrite the part below fully


The Ed25519 was introduced on OpenSSH version 6.5. It’s the EdDSA
implementation using the Twisted Edwards curve. It’s using elliptic curve
cryptography that offers a better security with faster performance compared to
DSA or ECDSA.

Today, the RSA is the most widely used public-key algorithm for SSH key. But
compared to Ed25519, it’s slower and even considered not safe if it’s generated
with the key smaller than 2048-bit length.

The Ed25519 public-key is compact. It only contains 68 characters, compared to
RSA 3072 that has 544 characters. Generating the key is also almost as fast as
the signing process. It’s also fast to perform batch signature verification
with Ed25519. It’s built to be collision resilence. Hash-function collision
won’t break the system.

:term:`SSH`.

Configuration
--------------------

.. todo::

  - Find the right configuration for server and the client
  - How to properly setup a rebound from one server to another

Key generation
--------------

.. code:: bash

  ssh-keygen -o -a 100 -t ed25519 -f ~/.ssh/id_ed25519 -C "john@example.com"

Useful commands
---------------

.. code:: bash

  # Copy public key on remote server
  ssh-copy-id -i <$HOME/.ssh/mykey> <user><host>

  # Remove host from known host
  ssh-keygen -R <hostname>

.. todo::

  - SSH key management : https://www.ssh.com/iam/ssh-key-management/
  - SSH key management soft: https://www.ssh.com/products/universal-ssh-key-manager/
