Docker: Containers
==================

.. todo::
  
    - Explanation about what is docker
    - how to use it
    - how to configure it
    - some useful commands

.. code-block:: bash

    # Remove one or several images
    docker image rmi <IMAGE_ID1> <IMAGE_ID2>

    # Remove all stopped containers, dandling images and unused networks and build cache
    docker system prune

    # Remove one or several containers
    docker container rm <CONTAINER_ID1> <CONTAINER_ID2>

    # List all stopped containers
    docker containers ls -a --filter status=excited --filter status=created

    # Remove all stopped containers
    docker container prune


Good practices
--------------

1. Prefer minimal base images: smaller surface attack
2. Least privileged user: nothing should run as root
3. Sign your images through `Docker Notary`_: mitigate MITM attacks
4. Find and fix vulnerabilities
5. Don't leak any sensitive information into the Docker image
6. Use fixed tags for immutability
7. Use COPY instead of ADD: mitigate MITM
8. Use LABEL for metadata
9. Use multi-stage building for small secure images
10. Use a linter



.. _Docker Notary: https://docs.docker.com/notary/getting_started/
