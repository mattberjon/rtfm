Ansible: automation at your fingertips
======================================

.. code-block:: bash

  # Start a new project
  mkdir -p </path/to/project/inventory> </path/to/project/roles>
  cd </path/to/project>

  # Create a README
  touch README.md <playbook_name>.yml;


- 1 small instance as a VDI (that may include a VPN)
- 1 instance to deploy few services


- create VDI
- configure it using Ansible
- create roles for:
  - base tools (vim, dig, netstats...)
  - motd
  - gitlab
  - DNS server
  - ...


Ansible Galaxy
--------------

Ansible Galaxy is an interesting tool that let you manage your roles and modules.
It basically help you to create a role or download all dependencies related to
a project.

.. code-block:: bash

  # Create a new role
  ansible-galaxy init <role-name>

  # List all available roles
  # Use the argument -p <roles_path> if your roles aren't in a known path
  # ~/.ansible/roles for example.
  ansible-galaxy list
