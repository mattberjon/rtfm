Bare metal setup
================

If you need to manage several machines, you need to have a basic setup and
a configuration manager. Here we use Ansible.

.. Note::
  
  Maybe the basic installation will differ depending if we install a
  physical machine, a virtual machine or a container.

Naming convention
-----------------

First things first, besides the fact that we'll need a DNS server to handle
the servers, it's still good to keep a naming convention for the servers.
They are many possitibilities and it will depend on several parameters like
the number of servers to handle, their location, etc. Some like to use names
related to a specific themes such as stars, Toy Story characters but in our
case we're going to be way more boring. Here what I suggest:

- service type and number (two digits)
- type designation: `p` for physical, `v` for virtual and `c` for container
- location: three letters based on the `IATA denomination`_
- the FQDN

As an example, a physical machine for monitoring based in Brussels would look
like: monitoring01.p.bru.example.net.


Base Operating System
---------------------

At the base of every IT information system exists an machine hosting an
:term:`OS`. We use exclusively open source software and there is plethora of
:term:`OS` out there with strenghs and weaknesses. Choose whatever fits the best
your needs either in the GNU/Linux or BSD world. Nevertheless always be aware
that a server is constantly under attack from script kiddies or more advanced
black hacker. Two things to keep in mind:

- Choose an OS that is robust enough for a server use
- keep it update to date in terms of security patches

I could add that the correct trade off between security and usability is always
difficult to find and shouldn't be taken lightely. In terms of server
configuration the balance should alway be in favor of security.

Let say that you chose which OS you want to install, it's now time to install
it. Several use cases can occur:

- You use a service provider and it will do the job of installing the OS for you
- You have the control over everything and in this case you'll either:

  - install the OS by hand yourself
  - have a ghost prepared and install copy it
  - install the OS through a :term:`TFTP` server
  - have something a bit more fancy to automate the process such as `OpenShift`_

Doing that by hand is cumbersome and even annoying. Nowadays you have plenty
of configuration management that will help you to do the job (and not do it
for you).
First thing first, at the OS installation, don't forget to add
either manually you SSH public key or have an automation taking care of that
(much better if
it's automated).


.. _IATA denomination: https://www.world-airport-codes.com
.. _OpenShift: https://docs.openshift.com/
