Documentation
=============

First thing first, you need to document what you are doing and keep track of
it over time. It's most cases neglected 

- What to document
- How to document
- Generate diagrams
- versionning
- the importance of dates

The importance of dates
^^^^^^^^^^^^^^^^^^^^^^^

Having the modification date on a document or even page by page can give an 
idea to the reader on how up to date is your document. In the case of a tool,
in most cases, after a year, you page will be fully deprecated. In the case of
a scientific research or methodology, chances your document will deprecate in
a longer period of time.

The README file
^^^^^^^^^^^^^^^

Any project must have a README file. It will describe the minimal information
necessary to understand the project, what it does, who did it, how to run it
and who to contact in terms of questions, issues or contributions.

- good practices for READMEs

.. |date| date::

Last Updated on |date|
