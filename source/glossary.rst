Glossary
========

.. glossary::
  
    DVCS
      Distributed Version Control System

    GPG
      Gnu Privacy Guard

    OS
      Operating System

    RTFM
      Read The Fucking Manual

    SSH
      Secure SHell

    TFTP
      Trivial File Transfer Protocol
