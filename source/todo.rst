TODO LIST
=========

.. todolist::

Misc
----
- Create a wifi network connected to a radius server for nominative identification
  on the network

Hardware provisionning
----------------------
Apprendre à déployer des OS sur des serveurs en masse
- maas: http://maas.io/

Hardware configuration
----------------------
Apprendre à configurer et gérer des équipements réseau en masse
- rconfig: https://www.rconfig.com/
- ansible?

Good practices
--------------

- development
  - git hooks
  - protected branch
  - bumping versionning







Use the wireless 4G as an AP for the network on DDwrt
-----------------------------------------------------

You need first to set up the WIFI hostspot on your phone (with a proper password).
Second, you need to setup your router using DDwrt.

What you have to do is using the physical wireless interface to connect to your
phone and create a second virtual interface that will represent your local
wireless network.

Here the configuration of of both of them:

.. code:: bash
  
  # Physical interface
  # in Wireless > Basic settings
  Wireless Mode: Repeater
  Wireless Network Mode: Mixed
  Wireless Network Name (SSID): <the name (SSID) of the phone hotspot>
  Sensitivity Range (ACK Timing): 2000
  Network Configuration: Briged

  # Virtual interface
  # in Wireless > Basic settings
  Wireless Network Name (SSID): <the name (SSID) of your local wireless network>
  Wireless SSID broadcast: Enable
  AP Isolation: Disable
  Network Configuration: Briged

Once done, do not forget to save it and apply the settings. Second phase, you
need to secure that. Go to Wireless > Wireless Security:

.. code:: bash

  # Physical interface
  Security Mode: WPA2-PSK
  WPA Algorithms: CCMP-128 (AES)
  WPA Shared Key: <password of your phone hotspot>
  Key Renewal Interval (in seconds): 3600

  # Virtual interface
  Security Mode: WPA2-PSK
  WPA Algorithms: CCMP-128 (AES)
  WPA Shared Key: <password for your local wireless network>
  Key Renewal Interval: 3600

Save the modifications and apply them. Your router should be able to connect
to your phone hotspot and enjoy your mobile network. 
