Introduction
============

This book comes from the need to learn a bunch of things about IT system 
administration, programming and other related subjects and put it under one
roof with a bit of coherence. Nowadays, information is scattered throughout the
internet and sometimes it's difficult to link all this information and 
understand the causalities or consequences. Moreover, we tend to store this
information in our brain somewhere and struggle to call it at the opportune
moment. This book is dedicated to all people like me.

Building an infrastructure from scratch is a real nightmare. Nowadays most
organisations rely on external service providers (which can have positive or
negative aspects). Nevertheless, when you start from scratch, it's difficult
to understand where to start. Let assume that we have the bare metal equipment
and it's properly connected. If you use a third party system like AWS or GPC,
this part is already done and you just have to enter what you need.

What do we need now? Be able to properly and quickly provision the machines.

Tools at first:

- one machine (what would be the requirements)
- git (to version some pieces of code required without the need for now of a
  proper server instance)

This machine will be configured for hosting the provisionning node for all
other bare metal machines.
