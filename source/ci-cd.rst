CI/CD: test and deploy
======================

Using Gitlab-CI system

If you want to deploy on a ad-hoc solution using your own server, there is no
API to handle it an easy way, there is still the possibility to perform this
operation using SSH. Here the process for pushing a static website on a remote
server:

.. code:: yaml

	# vim .gitlab-ci.yml
	#
	# If the ssh-agent isn't install we install the openssh-client
	# We'll need rsync to push the data through SSH to the remote server
	before_script:
		- 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
		- apt-get update -y && apt-get install -y rsync python3 python3-pip
		- mkdir -p ~/.ssh
		- eval $(ssh-agent -s)
		- '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
		- ssh-add <(echo "$PRODUCTION_PRIVATE_KEY")
		- echo "$PRODUCTION_PRIVATE_KEY" > ~/.ssh/id_rsa

This implies to create an SSH key pair without a passphrase. This key pair
will be specific to Gitlab and the User, we wouldn't like to give an attackant
to be able to access another service, virtual machine or physical server because
of that. Once done, you'll need to upload the public key on the server and
the add the private key as a variable of the CI system (in the CI/CD settings
of the project). Another variable will be the production server address 
containing the path:

- PRODUCTION_PRIVATE_KEY: the content of your private key
- PRODUCTION_SERVER: <username>@<hostname>:</path>


.. todo::

  - workflow using git-flow, protected branched
  - git hook to ensure
