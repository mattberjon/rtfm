Services list
=============

Learn to fully deploy and configure via ansible and/or terraform these
following services in a thoughful and correctly fashion. This mean making
coherent choices about the company architecture.

Maybe a good way to do so is to create a base image where I can import services
one by one and see later on how to integrate them step by step altogether.

Security - SIEM - Monitoring - Supervision
------------------------------------------

- `Splunk <https://www.splunk.com/>`_: SIEM
- `Grafana <https://grafana.com/>`_: Analytics dashboarding
- `Prometheus <https://prometheus.io/>`_: metrics monitoring and alerting
- `Cachet <https://cachethq.io/>`_: Status page
- `Zabbix <https://www.zabbix.com/>`_: Monitoring
- `OnCall <https://oncall.tools/docs/index.html>`_: On-call scheduling and calendar
- `Iris <https://iris.claims/>`_: Automated incident paging system
- `SNORT <https://www.snort.org/>`_: IPS
- `Fail2Ban <https://www.fail2ban.org/wiki/index.php/Main_Page>`: IPS

Virtualization - Containerization
---------------------------------

- `rancher <https://rancher.com/>`_: Kubernetes as a service
- `Docker <https://www.docker.com/>`_: Container technology
- `cloudstack <https://cloudstack.apache.org/>`_: cloud computing manager
- `RancherOS <https://rancher.com/rancher-os>`_: Minimal and secure Docker OS
- `CoreOS <https://coreos.com/>`_: Minimal and secure Docker OS
- `Clair <https://github.com/coreos/clair>`_: Vulnerability static analysis for containers
- `Klar <https://github.com/optiopay/klar>`_: CLI front-end for Clair
- `Clairctl <https://github.com/jgsqware/clairctl>`_: CLI front-end for Clair
- `reg <https://github.com/genuinetools/reg>`_: CLI and web font-end for Clair
- `anchore <https://anchore.com/opensource/>`_: Deep image inspection and vulnerability scanning
- `Sonobuoy <https://sonobuoy.io/>`_: Validate your kubernetes configuration
- `Cluster API <https://github.com/kubernetes-sigs/cluster-api>`_: Kubernetes style API

Backups - Restore
-----------------

- `Bacula <https://www.bacula.org/>`_: Backup software system
- `Victoria Metrics <https://victoriametrics.com/>`_: long terme time series database storage
- `Thanos <https://thanos.io/>`_: Highly available Prometheus with long term storage capabilities
- `Velero <https://velero.io/>`_: Backup and migrate Kubernetes resources and persistent volumes

Development
-----------

- `Gitlab <https://gitlab.com/>`_: DevOps system

Deployement
-----------

- `Ansible <https://www.ansible.com/>`_: automated deployement
- `Terraform <https://www.hashicorp.com/products/terraform/>`_: cloud infrastructure automation

Customer
--------

- `OStickets <https://osticket.com/>`_: ticketing system

Useful services
---------------

- `Seafile <https://www.seafile.com/en/home/>`_: File sync and share solution

Authentication - Authorization
------------------------------

- `Keycloak <https://www.keycloak.org/>`_: IAM
- `Freeradius <https://freeradius.org>`_: AAAA server
- `openLDAD <https://openldap.org/>`_: Directory access protocol implementation

Communication (synchronous or asynchronous)
-------------------------------------------

- `SpamAssassin <https://spamassassin.apache.org/>`_: spam filter
- `Roundcube <https://roundcube.net/>`_: webmail
- `Mattermost <https://mattermost.com/>`_: Instant messaging system
- `BigBlueButton <https://bigbluebutton.org/>`_: web conferencing (fully integrated to Mattermost )
- `Matrix <https://matrix.org/>`_: Open network protocol for decentrilize communications
- `Riot <https://riot.im/>`_: Instant messaging system based on Matrix
- `Jitsi Meet <https://jitsi.org/jitsi-meet/>`_: Visio conferencing system (possible integration with Riot)
- `Roundcube <https://roundcube.net/>`_: Webmail
- `opensmtpd <https://www.opensmtpd.org/>`_: SMTP server
- `SpamAssassin <https://spamassassin.apache.org/>`_: Spam filter
- `Sympa <https://www.sympa.org/>`_: Mailing list system
- `RockerChat <https://rocket.chat/>`_: Instant messaging system

Domain Name
-----------

- `NSD <https://www.nlnetlabs.nl/projects/nsd/about/>`_: DNS server with DNSSEC implementation

Machine learning
----------------

- `Hadoop <https://hadoop.apache.org/>`_: Framework for massively distributed computing
- `Pachyderm <https://www.pachyderm.com/>`_: Versionned pipeline framwork for big data/machine learning
- `Binder <https://mybinder.org/>`_: Setup an interactive notebook from a repo
- `JupyterHub <https://jupyter.org/hub>`_: Multi-user notebooks

Search
------

- elastic search
- `Solr <https://lucene.apache.org/solr/>`_: platform search

Databases
---------

- `Cassandra <https://cassandra.apache.org/>`_: NoSQL database
- postgresql: 
- MariaDB: 
- Redis:

To sort
-------

- `Nginx <https://nginx.org/>`_: HTTP server and much more
- `Consul <https://www.consul.io/>`_: Secure Service Networking
- `Vault <https://www.vaultproject.io/>`_: Manage Secrets and Protect Sensitive Data
- `Traefik <https://containo.us/traefik/>`_: Reverse proxy and load balancer for the cloud
- clonzilla: https://clonezilla.org/
- etcd: https://etcd.io/
- `mesos <https://mesos.apache.org/>`_: Cluster management
- GLPI: http://glpi-project.org/
- OCS inventory: https://ocsinventory-ng.org/?lang=en
- ELK: https://www.elastic.co/elastic-stack
- `fluentd <https://www.fluentd.org/>`_: Unified logging layer
- kibana: https://www.elastic.co/kibana
- statsd: https://github.com/statsd/statsd
- Kafka: https://kafka.apache.org/
- OpenVPN:
- StrongSWAN:
- Wireshark:
- HAProxy: https://www.haproxy.org/
- AlienVault OSSIM: https://cybersecurity.att.com/products/ossim
- `OSSEC <https://www.ossec.net/>`_: HIDS
- Metron: https://metron.apache.org/
- gestioip: http://www.gestioip.net/
- artifactory: https://jfrog.com/artifactory/
- EJBCA: https://www.ejbca.org
- `redmine <https://www.redmine.org/>`_: project management (with new theme: https://www.redmineup.com/pages/themes/circle)
- Chaos Monkey: https://netflix.github.io/chaosmonkey
- `k3OS <http://k3os.io/>`_: The Kubernetes Operating System
- `LongHorn <https://longhorn.io/>`_: Cloud native distributed block storage for Kubernetes
- `Werf <https://werf.io/>`_: Gitops CLI tool
- `Harbor <https://goharbor.io/>`_: cloud native repository for Kubernetes
- `Linkerd <https://linkerd.io/>`_: Ultralight, security-first service mesh for Kubernetes
- `Gatlin <https://gatling.io/>`_: developer tool to load test web apps
- `CloudFoundry <https://www.cloudfoundry.org/>`_: Cloud Foundry Lets You Create Apps, Not the Platform on Kubernetes
- `OpenStack <https://www.openstack.org/>`_: Open source software for creating private and public clouds
- `Packer <https://www.packer.io/>`_: Build Automated Machine Images
- `Graphite <http://graphiteapp.org/>`_: Store and graph metrics
- `Icinga <https://icinga.com/>`_: Availibility and performance monitoring
- `Rundeck <https://www.rundeck.com/>`_: Runbook automation
- `Graylog <https://www.graylog.org/>`_: Graylog management
- `Rudder <https://www.rudder.io/fr/logiciel/>`_: Solution de configuration continue
- `Mantis <https://www.mantisbt.org/>`_: Bug tracker
- `SonarQube <https://www.sonarqube.org/>`_: Code quality and security
- `Flyawaydb <https://flywaydb.org/>`_: Datbase migration system
- `Sensu <https://sensu.io/>`_: Monitoring for cloud environements
- `RackHD <https://rackhd.github.io/>`_: development tools and APIs to automate hardware management and orchestration
- `Portainer <https://www.portainer.io/>`_: Docker management
- `Spinnaker <https://www.spinnaker.io/>`_: Cloud native continuous delivery
- `ZAP <https://owasp.org/www-project-zap/>`_: Zed Attach Proxy, web security tool
- `Bugzilla <https://www.bugzilla.org/>`_: Bug tracking system
- `Cloudify <https://cloudify.co/>`_: Orchestration platform
- `Taiga <https://taiga.io/>`_: Project management tool
- `Helm <https://helm.sh/>`_: Package manager for Kubernetes
- `Yocto <https://www.yoctoproject.org/>`_: Framework to create custom linux-based systems
- `Tuleap <https://www.tuleap.org/>`_: Project management


Bare metal provisionning system
-------------------------------

- `maas <http://maas.io/>`_: Server provisionning
- `Xcat <http://xcat.org/>`_: Bare metal management software
- `Foreman <https://theforeman.org/>`_: lifecycle management 
- `FAI <https://fai-project.org/>`_: Fully Automatic Installation
- `OpenQRM <https://openqrm-enterprise.com/>`_: Professional Open-Source Data Center and Cloud Management
