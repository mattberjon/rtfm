RTFM
====

Welcome to Read The Fucking Manual, a documentation for many things related to
IT.

Installation
------------

This documentation is based on Sphynx. This assume that you have Python 3.x
installed on your machine and that you are able to generate a virtual
environment.

Just to help you out, here one way to do so from scratch:

.. code-block:: bash

   # Create the virtual environment
   python3 -m venv </path/to/new/virtual/environment>

   # Activate the virtual environment
   source </path/to/new/virtual/environment>/activate

   # Install the necessary tools for RTFM
   pip install -r requirements.txt

   # Build the book
   make html

TODO
----

Besides writing a cohenrent and pertinent book, here the following things to
do:

- have a nice LaTeX output
- be able to automatically generate nice diagrams in several formats
- have a testing pipeline
- add a bibliography
- add a glossary
